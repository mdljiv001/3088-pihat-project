The piHat that our group is designing is a UPS.
It allows the user to use a back up dc voltage source to power the Pi. The hat also has LED indicators to display the battery level so the pi can be shut down safely.

Bill of materials:

10 X 1k resistors  R20
3 X opamps R15
4 X PNP transistors R12
2 X Emosfets R30
1 X Voltage regulator LM7805 R35
